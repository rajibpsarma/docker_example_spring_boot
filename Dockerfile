FROM java:8

# Set the working directory.
WORKDIR /usr/src/app

# Copy the file required
COPY ./target/example-0.0.1-SNAPSHOT.jar spring-boot-app.jar

# Inform Docker that the container is listening on the specified port at runtime.
EXPOSE 8080

# Run the specified command within the container.
# i.e. java -jar spring-boot-app.jar
CMD ["java", "-jar", "spring-boot-app.jar"]