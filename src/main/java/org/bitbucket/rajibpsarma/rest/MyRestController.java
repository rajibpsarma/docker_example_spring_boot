package org.bitbucket.rajibpsarma.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest")
public class MyRestController {

	@GetMapping(value="/employees")
	public List<Employee> getEmployees() {
		List<Employee> data = new ArrayList<Employee>();
		data.add(new Employee(100, "Rajib P Sarma", "15A"));
		data.add(new Employee(200, "Raju Rastogi", "20B"));
		return data;
	}
}
