# Docker example using Spring Boot App #

An example of a simple Spring Boot app deployed to docker.

### The steps to be followed for building an Image and run a Container, are: ###
* Clone the repository used in this example using "git clone https://rajibpsarma@bitbucket.org/rajibpsarma/docker_example_spring_boot.git"
* Build the jar file using "mvn clean install"
* Build the docker image using "docker build -t spring-boot-app ."
* Verify the image using "docker images"
* Run the container using "docker run -p 8080:8080 --name my-spring-boot-app spring-boot-app"
* Verify container details using "docker ps"
* Access the spring boot application using "http://localhost:8080/"
* To remove the container use, "docker rm --force my-spring-boot-app"
* To remove the image use, "docker rmi --force spring-boot-app"

### The steps to be followed for pushing the docker image to Docker Hub, are: ###
* Sign into Docker Hub page (https://hub.docker.com/).
* Create a repository
* Build the docker image using "docker build -t <your docker hub user name>/<docker hub repository name> ."
* Verify the image using "docker images"
* Run the container using "docker run -p 8080:8080 <your docker hub user name>/<docker hub repository name>"
* Verify container details using "docker ps"
* Access the spring boot application using "http://localhost:8080/"
* To remove the container use, "docker rm --force <container id>"
* Log into Docker Hub from command prompt, using “docker login”. 
* We can push the local docker image to Docker Hub cloud by using "docker push <your docker hub user name>/<docker hub repository name>:<tagname>"
* Now, check the repository on Docker Hub page, it should display the pushed image with tag "latest".
* To remove the image use, "docker rmi --force <image id/name>"
* To fetch the docker image from Docker Hub to local, use "docker pull <your docker hub user name>/<docker hub repository name>:<tagname>".
* Now, this image can be used to run a container.